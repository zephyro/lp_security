$(".btn-modal").fancybox({
    'padding'    : 0
});

var myVideo = document.getElementById("myVideo");
myVideo.volume = 0;
myVideo.play();

$('.mute').click(function(e) {
    e.preventDefault();
    var mute = $(this).find('span');

    if (myVideo.volume > 0) {
        myVideo.volume = 0;
        mute.text('Включить звук');
    }
    else {
        myVideo.volume = 1;
        mute.text('убрать звук');
    }
});
